![Poilabs](https://www.poilabs.com/public/img/poi-labs-logo.png)
# PoilabsVdNavigationCore

Poilabs görme engelli navigasyon core sdk

## Yükleme

### CocoaPods
CocoaPods ile PoilabsVdNavigationCore sdk'ini projenize import edebilirsiniz.

```ruby
pod 'PoilabsVdNavigationCore', '~> 4.0.2'
```
## Öngereklilikler

### info.plist

Sdk'in çalışabilmesi için kullanıcıdan gerekli izinlerin alınması gerekmektedir. Bu izinler için info.plist'e aşağıdaki parametreler eklenmelidir.

+Privacy - Location Usage Description: ‘Sizin açıklamanız’

+Privacy - Location When In Use Usage Description: ‘Sizin açıklamanız’

+Privacy - Location Always Usage Description: ‘Sizin açıklamanız’

+Privacy - Location Always and When In Use Usage Description: ‘Sizin açıklamanız’

+Privacy - Bluetooth Peripheral Usage Description: ‘Sizin açıklamanız’

## Kullanım

### MainViewController

MainViewController AVM, Üniversite, Müze gibi Item'ların listelendiği controllerdır. Sdk init'i de burada yapılır. 

Bu controller'da Sdk ile iletişim PoiVDNavigationManager ile yapılır. 

Listelenecek Item'lar **ItemBase** tipindedir. 

#### ItemBase:

```swift
class ItemBase {
    var type: String? //Item'in tipini belirtir. Ornegin Avm, Üniversite, Müze
    var nameTr: String? //Item'in Turkce ismi
    var nameEn: String? //Item'in Ingilizce ismi 
}
```
#### Init ve getMenuItems:

PoiVDNavigationManager appId, secret ve uniqId ile init edilir. Bool bir response doner. Response true ise **getMenuItems** methodu ile item'lar alınabilir. 

```swift
init(configUrl: String? = nil,
    withApplicationID appID: String,
    withApplicationSecret secret: String,
    withUniqueIdentifier Id: String,
    completionHandler: @escaping (Bool) -> Void)
```


```swift
func getMenuItems() -> [ItemBase]
```

#### Kullanimlari:

```swift
import UIKit
import ios_vd_navigation_core
class MainViewController: UIViewController {
    var poiVDNavigation: PoiVDNavigationManager?
    var menuItems: [ItemBase] = []

        override func viewDidLoad() {
            super.viewDidLoad()
            ...
            poiVDNavigation = PoiVDNavigationManager(
                withApplicationID: "ApplicationID", 
                withApplicationSecret: "ApplicationSecret", 
                withUniqueIdentifier: "UniqueIdentifier") { (response) in
                    if response {
                        self.menuItems = self.poiVDNavigation?.getMenuItems() ?? []
                    }
            }
            ...
        }
}
```

#### MenuItem'larinin kullanımı: 

Alinan itemlar bir tableview ile listelenebilir.

```swift
public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
}
    
public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: avmCellId, for: indexPath) as! MainListTableViewCell
    let item = menuItems[indexPath.row]
    cell.lblTitle.text = item.nameTr
    return cell
}
```
#### Secilen item'in elementlerinin listelenmesi

Item'larin elementleri PlaceListViewController'da listelenebilir. PlaceListViewController parametre olarak ItemBase objesi alır.

```swift
public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let target = menuItems[indexPath.row]
    let listController = PlaceListViewController(target: target)
    navigationController?.pushViewController(listController, animated: true)
}

```

### PlaceListViewController

MainList'ten secilen item'in elementlerinin listelendiği ViewController'dir. ItemBase objesi ile init edilir. TableView ve TextField içerir.

Bu controllerda sdk ile iletişim PlaceListManager ile yapılır.

#### PlaceListManager: 

```swift
public class PlaceListManager {
    public init(target: String)
    public func getPlaceList() -> [Place]
    public func getPlaceFilteredList(withQuery query: String? = "") -> [FilteredPlace]
    public func filterMallListForChild(withParent parentTitle: String, placeList: [Place]) -> [FilteredPlace]
}
```

#### FilteredPlace ve Place:

```swift
public class FilteredPlace {
    public var city: String
    public var placeList: [Place]
    public init(withCity city: String)
}


```
#### Kullanimlari:

```swift
import UIKit
import ios_vd_navigation_core
class PlaceListViewController: UIViewController {
	var target: ItemBase?
	var placeList: [Place] = []
	var placeFilteredList: [FilteredPlace] = []
	var placeListManager: PlaceListManager?

	@IBOutlet weak var searchBarTextField: UITextField!
	@IBOutlet weak var tablePlaces: UITableView!

    convenience init(target: ItemBase) {
        self.init()
        self.target = target
        placeListManager = PlaceListManager(target: target.type!)
    }

    override func viewDidLoad() {
        ...
        placeList = placeListManager?.getPlaceList() ?? []
        placeFilteredList = placeListManager?.getPlaceFilteredList() ?? []

        searchBarTextField.addTarget(self, action: #selector(handleSearchBarValueChange(_:)), for: .editingChanged)
        ...
    }

	// searchbar'a girilen karakterlere gore list'i gunceller
    @objc func handleSearchBarValueChange(_ tf : UITextField){
        placeFilteredList = placeListManager?.getPlaceFilteredList(withQuery: tf.text!) ?? []
        tablePlaces.reloadData()
    }
}
```


#### PlaceList Item'larinin tableView ile listelenmesi:

```swift
extension PlaceListViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return placeFilteredList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (placeFilteredList[section].placeList.count)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: avmCellId, for: indexPath) as! AvmListTableViewCell
        if let title = placeFilteredList[indexPath.section].placeList[indexPath.item].title {
            cell.lblTitle.text = title
        }
        if let subtitle = placeFilteredList[indexPath.section].placeList[indexPath.row].address {
            cell.lblSubtitle.text = subtitle
        } else {
            cell.lblSubtitle.text = placeFilteredList[indexPath.section].placeList[indexPath.row].description
        }
        return cell
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: avmHeaderId) as! GeneralTableHeaderView
        header.titleText = placeFilteredList[section].city
        return header
    }
}
```

#### PlaceList Item'in detayina gecis:

Detaya geciste iki ihtimal bulunmaktadir. 

 - Place'in child'i varsa
 - Place'in child'i yoksa

Bir place secildigi zaman child kontrolu yapilmalidir. Eger childi varsa placeList ve placeFilteredList place'in child'ina gore guncellenmelidir. Child yok ise PlaceDetailViewController'a gecilmelidir.


```swift
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let ID = placeFilteredList[indexPath.section].placeList[indexPath.item].id {
            //child yoksa detaya git
            if (placeFilteredList[indexPath.section].placeList[indexPath.item].childs == nil) {
                let detailController = PlaceDetailViewController(ID, target: placeFilteredList[indexPath.section].placeList[indexPath.item])
                navigationController?.pushViewController(detailController, animated: true)
            } else {
                //child'ini goster
                placeList = placeFilteredList[indexPath.section].placeList[indexPath.item].childs!
                placeFilteredList = (placeListManager?.filterMallListForChild(withParent: placeFilteredList[indexPath.section].placeList[indexPath.item].title!, placeList: placeList))!
                tablePlaces.reloadData()
            }
        }
    }

```

### PlaceDetailViewController

Secilen place icindeki pointlerin listelendigi, serbest dolasimin kullanilabildigi ve place hakkinda info alinabildigi controllerdir.

Bu controllerda sdk ile iletisim PlaceDetailManager ile yapilir.

Serbest dolasim icin sdk ile iletisim NavigationManager ile yapilir.

#### PlaceDetailManager:

```swift
public class PlaceDetailManager {
	public init(placeId: String, completionHandler: @escaping (Bool) -> Void)
	public func getInfoText() -> String
	public func getPointList() -> [FilteredPoi]
}

public class NavigationManager : NSObject {
    public init(target: Place)
    public var delegate: NavigationManagerDelegate?
}

```

#### Kullanimlari:

```swift
class PlaceDetailViewController: UIViewController {
	var placeDetailManager: PlaceDetailManager?
	var navigationManager: NavigationManager?
    var directionText = ""
    var infoText = ""
    var target: Place?
	let mLocationView = DetailsFreeWalkView()

    convenience init(_ id: String, target: Place, infoText: String? = nil) {
        self.init()
        self.target = target
        if let infoText = infoText {
            self.infoText = infoText
        }
        placeDetailManager = PlaceDetailManager(placeId: id, completionHandler: { (response) in
            let mInfoView = DetailsInfoView()
            mInfoView.infoString = self.placeDetailManager?.getInfoText()
            self.infoView.addSubview(mInfoView)
            let mListView = DetailsTableView()
            mListView.detailController = self
            mListView.parent = self
            mListView.targetController = nil
            mListView.placeDetailManager = self.placeDetailManager
            mListView.allPoints = self.placeDetailManager?.getPointList()
            self.listView.addSubview(mListView)
            self.locationView.addSubview(self.mLocationView)
        })
        
        navigationManager = NavigationManager(target: self.target!)
    }


    override func viewDidAppear(_ animated: Bool) {
	...
        navigationManager?.delegate = self
    	...
	}

    override func viewDidDisappear(_ animated: Bool) {
    	...
	   navigationManager?.delegate = nil
    	...
	}

}
```

#### MavigationManagerDelegate: 
```swift
extension PlaceDetailViewController: NavigationManagerDelegate {
    func navigationManagerDidUpdateUserLocation(nodeList: [Poi], heading: CLHeading) {
    }
    
    func navigationManagerUserLocationText(userLocationText: String) {
        if directionText != userLocationText {
            directionText = userLocationText
            mLocationView.infoString = directionText
        }
    }
}
```

#### DetailsTableView:

```swift
class DetailsTableView: UIView {
    var placeDetailManager: PlaceDetailManager?
    var detailController: PlaceDetailViewController?
    var targetController: PointDetailViewController?
    var navigationController: NavigationViewController?
    weak var parent: PlaceDetailViewController?
    var filteredList:[FilteredPoi] = []
    var allPoints:[FilteredPoi]? {
      didSet {
        tableMalls = UITableView(frame: CGRect(x: 0, y: 100, width: self.frame.width, height: self.frame.height - 100))
        tableMalls.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(tableMalls)
        filteredList = allPoints!
        getTypeList()
      }
    }

    func getTypeList() {
      var types : [String] = ["Tümü"]
      for poi in allPoints! {
        if poi.description != "" {
            if (types.filter({return $0 == poi.description}).count) == 0 {
              types.append(poi.description)
            }
          }
      }
      storeTypes = types
    }

    func shouldAddList(withPoi poi : Poi , _ keyword : String) -> Bool {
      if keyword.count <= 0 {
        return true
      }
      else{
        if poi.title?.lowercased().range(of: keyword.lowercased()) != nil {
          return true
        }
      }
      return false
    }

    @objc func textFieldDidChange(_ textField : UITextField){
        if let keyword = textField.text {
            filteredList = filterList(withKeyword: keyword)
            tableMalls.reloadData()
        }
    }

    func filterList(withKeyword keyword: String) -> [FilteredPoi] {
        if keyword == "" {
            return allPoints ?? []
        } else {
            return allPoints?.filter({$0.title.contains(keyword)}) ?? []
        }
    }

}

extension DetailsTableView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return filteredList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: shopCellId, for: indexPath) as! PointListTableViewCell
        cell.lblFloor.text = filteredList[indexPath.item].floor
        cell.lblTitle.text = filteredList[indexPath.item].title
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = filteredList[indexPath.item]
        if item.title == "Tuvalet" || item.title == "Asansör" {
            navigationController = NavigationViewController(destinationPoi: item)
            detailController?.navigationController?.show(navigationController!, sender: self)
        } else {
            targetController = PointDetailViewController(point: item)
            detailController?.navigationController?.show(targetController!, sender: self)
        }
    }
}

```

### PointDetailViewController

Secilen pointin detaylarinin yazdigi, navigasyon alinabildigi ve varsa iceriginin okunabildigi viewcontroller
```swift
class PointDetailViewController: UIViewController {
	var poi: FilteredPoi?
    convenience init(point: FilteredPoi) {
        self.init()
        self.poi = point
    }

    override func viewDidLoad() {
        super.viewDidLoad()
		...
        if (poi?.content == nil) {
            btnContent.isHidden = true
        }
        self.title = poi?.title
        if let title = poi?.title {
            lblAvm.text = title
        }
        if let title = poi?.title,
            let desc = poi?.description,
            let floor = poi?.floor {
            lblAdress.text = "\(title), \(floor), \(desc)"
        }
		...
    }
}
```

#### Navigasyon alma ve icerik okuma
```swift
    @IBAction func btnNavigation_Click(_ sender: Any) {
        let detailController = NavigationViewController(destinationPoi: poi!)
        navigationController?.pushViewController(detailController, animated: true)
    }

    @IBAction func readContent(_ sender: Any) {
        labelContent.attributedText = poi?.content?.html2AttributedString
    }
```


### NavigationViewController
```swift
class NavigationViewController: UIViewController {

    @IBOutlet weak var navigationLabel: UILabel!
    var routeManager: RouteManager?
    var destinationPoi: FilteredPoi?
    
    convenience init(destinationPoi: FilteredPoi) {
        self.init()
        self.destinationPoi = destinationPoi
        routeManager = RouteManager(destinationPoi: destinationPoi)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        routeManager?.delegate = self
    }
}
```

#### RouteManagerDelegate:

```swift
extension NavigationViewController: RouteManagerDelegate {
    
	func routeManagerStepUpdate(navigationText: String) {
        navigationLabel.text = navigationText
    }
    
    func routeManagerPopUp(popUpMessage: String) {
        let alert = UIAlertController(title: "Rota", message: popUpMessage, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))

        self.present(alert, animated: true)
    }
}
```



